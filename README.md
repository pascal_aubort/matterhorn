Opencast Matterhorn
===================

Open Source Lecture Capture & Video Management for Education

Matterhorn is a free, open-source platform to support the management of
educational audio and video content. Institutions will use Matterhorn to
produce lecture recordings, manage existing video, serve designated
distribution channels, and provide user interfaces to engage students with
educational videos.


Installation
------------

Installation instructions can be found at:

* [Matterhorn Release Docs for Development Branch](https://opencast.jira.com/wiki/display/MHTRUNK/Install)


Community
---------

More information about the community can be found at:

* [Opencast Community Website](http://opencast.org/)
* [Mailing lists, IRC, …](http://opencast.org/communications)
* [Issue Tracker](http://opencast.jira.com/)


https://codeship.com/projects/b48aab30-e395-0132-c43f-6621228c1f6a/status?branch=master